@include('include.header')

<title>Add User</title>

<h1>Add User</h1>
<!-- register form start -->
<form method="POST" action="/padduser" enctype="multipart/form-data">
    {{csrf_field()}}

        <div>
        {{$errors->first('u_name')}} 
        </div>

        <div>
        <input id="u_name" name="u_name" type="text" value="{{ old('u_name') }}" placeholder="Enter nameUser Name here">
        </div>

        <div>
        {{$errors->first('name')}} 
        </div>

        <div>
        <input id="name" name="name" type="text" value="{{ old('name') }}" placeholder="Enter name here">
        </div>

        <div>
        {{$errors->first('email')}} 
        </div>

        <div>
        <input id="email" name="email" type="email" value="{{ old('email') }}" placeholder="Enter your email here">
        </div>

        <div>
        {{$errors->first('phone_number')}} 
        </div>

        <div>
        <input id="phone_number" name="phone_number" value="{{ old('phone_number') }}" type="number" placeholder="Enter Phone Number here">
        </div>

        <div>
        {{$errors->first('address')}} 
        </div>

        <div>
        <input id="address" name="address" type="text" value="{{ old('address') }}" placeholder="Enter Address here">
        </div>

        <div>
        {{$errors->first('u_type')}} 
        </div>

        <div>
        <select id="u_type" name="u_type" type="text">
        <option value="not_selected" selected>Select Type</option>
        <option value="Marketing" >Marketing</option>
        <option value="Internship">Internship</option>
        <option value="Employee">Employee</option>
        <option value="Project">Project</option>
        <option value="Accounts">Accounts</option>
        <option value="Franchise">Franchise</option>
        <option value="Think Innovator Circle">Think Innovator Circle</option>
        <option value="Education">Education</option>
        <option value="TAL Website">TAL Website</option>
        <option value="Incubation">Incubation</option>
        </select> 
        </div>

        <div>
        {{$errors->first('password')}} 
        </div>

        <div>
        <input id="password" name="password" type="password" placeholder="Enter password here">
        </div>

        <div>
        {{$errors->first('image')}} 
        </div>

        <div>
        <input type="file" value="{{ old('image') }}" name="image"> 
        </div>
        
        <div>
        <button type="submit" class="btn btn-primary">Submit</button> 
        </div>

</form>    
<!-- register form end -->

<!-- error handaler -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- flash massage show -->
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
@endif

@include('include.footer')