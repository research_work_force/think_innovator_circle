@include('include.header')

<form method="POST" action="/pdocs" enctype="multipart/form-data">
{{csrf_field()}}
{{$errors->first('name')}}


    <div>
    <input id="name" name="name" type="text" value="{{ old('name') }}" placeholder="Enter File Title here">
    </div>
    
    <div>
    {{$errors->first('pdf')}} 
    </div>

    <div>
    <input type="file" value="{{ old('pdf') }}" name="pdf"> 
    </div>
        
    <div>
    <button type="submit" class="btn btn-primary">Submit</button> 
    </div>

</form>


<div>
<table class="table table-hover">

<thead>

  <th>name</th>

  <th>Action</th>

</thead>

<tbody>
@foreach($docs_show as $docs)
    <tr>

      <td>{{$docs->name}} </td>

    <td> 
      <a class="btn btn-sm btn-info" href="{{ asset('storage/pdf/'. $docs->pdf) }}">View</a>
      <a class="btn btn-sm btn-primary" href="/edit/pdf/{{$docs->id}}">Edit</a>
      <a class="btn btn-sm btn-danger" href="/delete-pdf/{{$docs->id}}">Delete</a>
      </td>
      </tr>
@endforeach

</tbody>

</table>

</div>

<!-- flash massage show -->
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
@endif


    

@include('include.footer')            