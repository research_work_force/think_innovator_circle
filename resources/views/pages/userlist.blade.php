@include('include.header')

<div>
<a class="btn btn-info" href="/adduser">Add User</a>
<a class="btn btn-success" href="{{route('export_excel.excel')}}">Download in Excel</a>
<a class="btn btn-success" href="{{route('export_csv.csv')}}">Download in CSV</a>
<a class="btn btn-success" href="{{route('export_pdf.pdf')}}">Download in PDF</a>
</div>


<div>
<table class="table table-hover">

<thead>

  <th>Profile Picture</th>

  <th>User's name</th>

  <th>Phone Number</th>

  <th>Email Id</th>

  <th>Address</th>
 
  <th>Type</th>

  <th>Action</th>

</thead>

<tbody>
@foreach($users as $user)
    <tr>

      <td>  <img src="{{ asset('storage/images/'. $user->image)  }}" alt="image" width="100px"> 
      </td>

      <td>{{$user->name}} </td>

      <td>{{$user->phone_number}} </td>

      <td>{{$user->email}} </td>

      <td>{{$user->address}} </td>

      <td>{{$user->u_type}}</td>

      <td>
      <a class="btn btn-sm btn-info" href="/edit-prof/{{$user->id}}">Edit</a>
      <a class="btn btn-sm btn-danger" href="/delete-prof/{{$user->id}}">Delete</a>
      </td>
      </tr>
@endforeach

</tbody>

</table>

</div>

<!-- flash massage show -->
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
@endif

    

@include('include.footer')            