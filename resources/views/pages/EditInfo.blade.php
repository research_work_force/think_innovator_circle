@include('include.header')

<form method="POST" action="/pedit/save/{{$user->id}}" enctype="multipart/form-data">
    {{csrf_field()}}

        <div>
        <input id="id" name="id" type="hidden" value= "{{$user->id}}">
         </div>

        <div>
        {{$errors->first('name')}} 
        </div>

        <div>
        <input id="name" name="name" type="text" value= "{{$user->name}}"  placeholder="Enter Name here">
         </div>

        <div>
        {{$errors->first('email')}} 
        </div>

        <div>
        <input id="email" name="email" value= "{{$user->email}}" type="email" placeholder="Enter Email ID">
         </div>

        <div>
        {{$errors->first('phone_number')}} 
        </div>

        <div>
        <input id="phone_number" name="phone_number" value= "{{$user->phone_number}}" type="number" placeholder="Enter phone number">
         </div>

        <div>
        {{$errors->first('address')}} 
        </div>

        <div>
        <input id="address" name="address" value= "{{$user->address}}" type="text" placeholder="Enter address">
         </div>

        <div>
        {{$errors->first('u_type')}} 
        </div>

        <div>
                
            <select name="u_type" type="text" >
            <option value="{{$user->u_type}}" selected>{{$user->u_type}}</option>
            <option value="Marketing">Marketing</option>
            <option value="Internship">Internship</option>
            <option value="Employee">Employee</option>
            <option value="Project">Project</option>
            <option value="Accounts">Accounts</option>
            <option value="Franchise">Franchise</option>
            <option value="Think Innovator Circle">Think Innovator Circle</option>
            <option value="Education">Education</option>
            <option value="TAL Website">TAL Website</option>
            <option value="Incubation">Incubation</option>
            </select> 
       
        </div>     

        <div>
        <input id="u_name" name="u_name" type="hidden" value= "{{$user->u_name}}" >
         </div>
    
        <div>
        <img src="{{ asset('storage/images/'. $user->image)  }}" alt="image" width="100px"> 
        </div>

        <div>
        {{$errors->first('image')}} 
        </div>

        <div>
        <input type="file" name="image"> 
        </div>
        
        <div>
        <button type="submit" class="btn btn-primary">Submit</button> 
        </div>

</form>

@include('include.footer')