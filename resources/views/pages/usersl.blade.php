@include('include.header')

<table class="table table-bordered">
  <thead>
    <tr>
    <th>User's name</th>
    <th>Phone Number</th>
    <th>Email Id</th>
    <th>Address</th>
    <th>Type</th>
    </tr>
  </thead>
  <tbody>
  @foreach($users as $user)
    <tr>
    <td>{{$user->name}} </td>
    <td>{{$user->phone_number}} </td>
    <td>{{$user->email}} </td>
    <td>{{$user->address}} </td>
    <td>{{$user->u_type}}</td>
    </tr>
    @endforeach
  </tbody>
</table>

@include('include.footer')       