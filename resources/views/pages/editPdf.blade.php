@include('include.header')

<form method="POST" action="/pedit/pdf/{{$editPdf->id}}" enctype="multipart/form-data">
{{csrf_field()}}
{{$errors->first('name')}}


    <div>
    <input id="name" name="name" type="text" value="{{ $editPdf->name }}" placeholder="Enter File Title here">
    </div>
    
    <!-- <div>
    <img src="{{ asset('storage/images/'. 'pdf-thumbnail.png')  }}"  width="100px"> 
    </div> -->

    <div>
    <iframe src="{{ asset('storage/pdf/'. $editPdf->pdf) }}" width="10%"></iframe>
    </div>

    <div>
    {{$errors->first('pdf')}} 
    </div>

    <div>
    <input type="file" name="pdf"> 
    </div>
        
    <div>
    <button type="submit" class="btn btn-primary">Submit</button> 
    </div>

</form>


<!-- flash massage show -->
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
@endif


    

@include('include.footer') 