@include('include.header')

<title>Login</title>

<h1>Login</h1>
<!-- login form start -->
<form method="POST" action="/palogin">
    {{csrf_field()}}

        <div>
        <input id="email" name="email" type="email" placeholder="Enter your Email here">
        </div>

        <div>
        <input id="password" name="password" type="password" placeholder="Enter your password here">
        </div>
        
        <div>
        <button type="submit" class="btn btn-primary">Log in</button> 
        </div>

</form>    
<!-- login form end -->

        <div>
        <a  href="/ulogin">User Log in</a> 
        </div>

<!-- error handaler -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- flash massage show -->
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
@endif

@include('include.footer')