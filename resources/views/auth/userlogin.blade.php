@include('include.header')

<title>Login</title>

<h1>Login</h1>
<!-- login form start -->
<form method="POST" action="/pulogin">
    {{csrf_field()}}

        <div>
        <input id="u_name" name="u_name" type="text" placeholder="Enter your User Name here">
        </div>

        <div>
        <input id="password" name="password" type="password" placeholder="Enter your password here">
        </div> 
        
        <div>
        <button type="submit" class="btn btn-primary">Log in</button> 
        </div>

</form>    
<!-- login form end -->

        <div>
        <a href="/alogin">Admin Log in</a> 
        </div>

<!-- error handaler -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- flash massage show -->
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
@endif

@include('include.footer')