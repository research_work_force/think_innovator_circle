@include('include.header')

<title>Register</title>

<h1>Register</h1>
<!-- register form start -->
<form method="POST" action="/preg">
    {{csrf_field()}}


        <div>
        <input id="email" name="email" type="email" placeholder="Enter your email here">
        </div>

        <div>
        <input id="password" name="password" type="password" placeholder="Enter your password here">
        </div>

        <div>
        <input id="confirmed-password" name="confirmed_password" type="password" placeholder="Re-Enter your password here">
        </div>
        
        <div>
        <button type="submit" class="btn btn-primary">Submit</button> 
        </div>

</form>    
<!-- register form end -->

<!-- error handaler -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- flash massage show -->
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
@endif

@include('include.footer')