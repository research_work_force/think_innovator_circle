@include('include.header')

<html>
   
   <head>
      <title>View Student Records</title>
   </head>
   
   <body>
      <table border = 1>
         <tr>
            <td>Name</td>
            <td>Phone Number</td>
            <!-- <td>Email</td> -->
            <td>Action</td>
         </tr>
         
         <tr>
          
            <td>{{$user->name}}</td>
            <td>{{$user->phone_number}}</td>
            <!-- <td>{{$user->email}}</td> -->
            <td>
            <a type="button" class="btn btn-sm btn-info" href="/edit-prof">Edit</a>
            </td>
         </tr>
         
      </table>
   </body>
</html>   

<!-- error handaler -->
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- flash massage show -->
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
@endif



@include('include.footer')            