<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route for User login
Route::get('/ulogin', 'AuthController@LoginView' );
Route::post('/pulogin', 'AuthController@LoginSubmit' );
Route::get('/udash',  function () {
    return view('pages.udash');
});

// route for Admin login
Route::get('/alogin', 'AuthController@AdminLoginView' );
Route::post('/palogin', 'AuthController@AdminLoginSubmit' );
Route::get('/adash',  function () {
    return view('pages.adash');
});

//route for logout for User
Route::get('/ulogout', 'AuthController@Logout');


//route for logout for Admin
Route::get('/alogout', 'AuthController@AdminLogout');

//route for user list
Route::get('/userlist', 'AuthController@ShowUserlist');

//route for add user
Route::get('/adduser','AuthController@AddUser');
Route::post('/padduser', 'AuthController@AddUserSubmit');

//route for edit key
Route::get('/edit-prof/{id}', 'AuthController@edit_info');

// route for save edited form
Route::get('/edit/save/{id}', 'AuthController@edit_save' );
Route::post('/pedit/save/{id}', 'AuthController@edit_save' );

//route for delete user
Route::get('/delete-prof/{id}', 'AuthController@delete_info');

//route for docs
Route::get('/docs',   function () {
    return view('pages.docs');
});
//show list of pdf
Route::get('/docs', 'AuthController@list_pdf');
//add pdf
Route::post('/pdocs', 'AuthController@add_pdf');
//delete pdf
Route::get('/delete-pdf/{id}', 'AuthController@delete_pdf');
// edit pdf
Route::get('/edit/pdf/{id}', 'AuthController@edit_infopdf' );
Route::post('/pedit/pdf/{id}', 'AuthController@edit_pdf' );
//download data in exel
Route::get('/excel', 'AuthController@excel')->name('export_excel.excel');
// Export to csv
Route::get('/articles/exportCSV','AuthController@exportCSV')->name('export_csv.csv');
//Export in pdf
Route::get('/pdf','AuthController@generatePDF')->name('export_pdf.pdf');



// // route for login
// Route::get('/login', 'AuthController@LoginView' );
// Route::post('/plogin', 'AuthController@LoginSubmit' );
// Route::get('/dashboard',  function () {
//     return view('dashboard');
// });
// //route for show own profile
// Route::get('/prof', 'AuthController@index');


// // route for save edited form
// Route::get('/edit/save', 'AuthController@store' );
// Route::post('/pedit/save', 'AuthController@store' );

// Route::get('/edit-prof', 'AuthController@edit_info');



// route for register
// Route::get('/reg', 'AuthController@RegisterView' );
// Route::post('/preg', 'AuthController@RegisterSubmit' );






// <div>
// <a href="/adduser">Add New Documents</a>
// </div>


// <div>
// <table class="table table-hover">

// <thead>

//   <th>name</th>

//   <th>Action</th>

// </thead>

// <tbody>
// @foreach($users as $user)
//     <tr>

//       <td>{{$user->name}} </td>

//         <td>  <img src="{{ asset('storage/images/'. $user->image)  }}" alt="image" width="100px"> 
//       <a class="btn btn-sm btn-info" href="/edit-prof/{{$user->id}}">Edit</a>
//       <a class="btn btn-sm btn-danger" href="/delete-prof/{{$user->id}}">Delete</a>
//       </td>
//       </tr>
// @endforeach

// </tbody>

// </table>

// </div>

// <!-- flash massage show -->
// @if(Session::has('message'))
// <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
// @endif
