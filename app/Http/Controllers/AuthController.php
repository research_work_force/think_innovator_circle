<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Users;
use App\Admin;
use App\Login;
use App\Docs;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Excel;
use App\Exports\UsersExport;
use PDF;

class AuthController extends Controller
{   
            //User login controllers start
            public function LoginView(){
                return view('auth.userlogin');
                }

            public function LoginSubmit(Request $request){
        
                // validation checker
            $validatedData = $request->validate([        
                'u_name' => 'required|max:255',
                'password' => 'required|min:6',
            ]);

            // save in variable
            $u_name = $request->input('u_name');
            $password = $request->input('password');
            $hashpassword = Hash::make($password);
        
            //check in database of login
            $login_users = Login::where("u_name",$u_name)->first();
            
            if(empty($login_users)) {         

                \Session::flash('message', "You enter wrong User Name"); 
                \Session::flash('alert-class', 'alert-danger'); 

                return redirect('/ulogin');
            }
            else{
                                
                if(Hash::check($password,$login_users['password'])) { 

                    \Session::flash('message', "You are Loged In"); 
                    \Session::flash('alert-class', 'alert-success');
                    session(['u_name' => $u_name]); 
                    return redirect('/udash');
                }
                else{    
                    \Session::flash('message', "You enter wrong Password"); 
                    \Session::flash('alert-class', 'alert-danger'); 
        
                    return redirect('/ulogin');    
                    
                    }
                
                }
            }
            //User login controller end


             //Admin login controllers start
         public function AdminLoginView(){
            return view('auth.adminlogin');
            }

        public function AdminLoginSubmit(Request $request){
    
            // validation checker
        $validatedData = $request->validate([        
            'email' => 'required|max:255',
            'password' => 'required|min:6',
        ]);

        // save in variable
        $email = $request->input('email');
        $password = $request->input('password');
        $hashpassword = Hash::make($password);
    
        //check in database of Admin
        $login_admin = Admin::where("email",$email)->first();
        
        if(empty($login_admin)) {         

            \Session::flash('message', "You enter wrong Email Id"); 
            \Session::flash('alert-class', 'alert-danger'); 

            return redirect('/alogin');
        }
        else{
                            
            if(Hash::check($password,$login_admin['password'])) { 

                \Session::flash('message', "You are Loged In"); 
                \Session::flash('alert-class', 'alert-success');
                session(['email' => $email]); 
                return redirect('/adash');
            }
            else{    
                \Session::flash('message', "You enter wrong Password"); 
                \Session::flash('alert-class', 'alert-danger'); 
    
                return redirect('/alogin');    
                
                }
            
            }
        }
        //Admin login controller end

        // logout controller for User start
        public function Logout(){
            \Session::flush();

            return redirect('/ulogin');
            }
        //logout controller for User end

        // logout controller for Admin start
        public function AdminLogout(){
            \Session::flush();

            return redirect('/alogin');
            }
        //logout controller for Admin end

        //user list controller start
        public function ShowUserlist(){

            $users = Users::all();
            return view('pages.userlist', compact('users'));
        }
    
        //user list controller end

        //add user to list
   public function AddUser(){
    return view('pages.adduser');
}

public function AddUserSubmit(Request $request){
 
             // validation checker
             $rules = [
             'name' => 'required|min:5|max:55',
             'email' => 'required|unique:users|max:255',
             'phone_number' => 'nullable|numeric',
             'address' => 'nullable|max:225',           
             'u_name' => 'required|unique:users|unique:login',
             'u_type' => 'required',
             'image' => 'nullable|mimes:jpeg,png,jpg',
             'password' => 'required|min:6',
            ];
            $error_massage = [
                'name.required' => 'Name Is Important, Enter It',
                'name.min:5' => 'Name Is Very Short',
                'email.required' => 'Email Is Important, Enter It',
                'phone_number.numeric' => 'Phone Number Should Be Number',
                'address.max:225' => 'Address Is Very Long Make It Short',
                'u_name.required' =>'User Name Is Very Importent, Enter It',
                'u_name.unique:users' => 'This User Name Already Taken, Choose Another One',
                'u_name.unique:login' => 'This User Name Already Taken, Choose Another One',
                'image.mimes:jpeg,png,jpg' => 'You Should Be Choose A jpg,jpeg or png File',
                'password.required' => 'Password Is Important Like User Name',
                'password.min:6' => 'Password Should Be More Than Six Character'

            ];    
            
            $this->validate($request, $rules, $error_massage);


         // save in variable
         $name = $request->input('name');
         $email = $request->input('email');
         $phone_number = $request->input('phone_number');
         $address = $request->input('address');
         $u_name = $request->input('u_name');
         $u_type = $request->input('u_type');
         $password = $request->input('password');
         $hashpassword = Hash::make($password);
     
         //save in database of user
         $users = Users::where("u_name",$u_name)->first();
         
         if(empty($users)) {  
             $user = new Users();
             $user->name = $name;
             $user->email = $email;
             $user->phone_number = $phone_number;
             $user->address = $address;
             $user->u_name = $u_name;
             $user->u_type = $u_type;
             if ($request->hasFile('image')) {
                $images = $request->file('image');
                $filename = time() . '.' . $images->getClientOriginalExtension();
                $path = $request->image->storeAs('public/images', $filename);
            }else{
                $filename = 'no-image.png';
            }
            $user->image = $filename;
            $user->save();

             $login_user = new Login();
             $login_user->u_name = $u_name;
             $login_user->password = $hashpassword;
             $login_user->save();

             \Session::flash('message', "Account has been created"); 
             \Session::flash('alert-class', 'alert-success'); 

             return redirect('/userlist');
         
            }else{

             \Session::flash('message', "The User Name already taken");
             return redirect()->back();
            }
             
     
     
 }
 // Add user controllers end

//edit info controllers start
public function edit_info($id) {

    $users=Users::where('id', $id)->first(); 
    // $users = Users::where('u_name', $u_name)->first();
    return view('pages.EditInfo')->with('user', $users);
 }




 public function edit_save(Request $request, $id){

    // $demail = Users::where('id', $id)->pluck('email')->delete;
    // echo $demail;die;
    
    // validation checker
    $rules = [
        'name' => 'required|min:5|max:55',
        'email' => 'required|unique:users,id,.$id|max:255',
        'phone_number' => 'nullable|numeric',
        'address' => 'nullable|max:225',    
        'u_type' => 'required',
        'image' => 'nullable|mimes:jpeg,png,jpg'
       ];
       $error_massage = [
           'name.required' => 'Name Is Important, Enter It',
           'name.min:5' => 'Name Is Very Short',
           'email.required' => 'Email Is Important, Enter It',
           'phone_number.numeric' => 'Phone Number Should Be Number',
           'address.max:225' => 'Address Is Very Long Make It Short',
           'image.mimes:jpeg,png,jpg' => 'You Should Be Choose A jpg,jpeg or png File'

       ];    
       
       $this->validate($request, $rules, $error_massage);

    //save in database of user
        $abc = Users::where('id', $id)->first();

        // save in variable
        // $inp_id=$request->input('id');
        $name = $request->input('name');
        $address = $request->input('address');
        $email = $request->input('email');
        $u_type = $request->input('u_type');
        $phone_number = $request->input('phone_number');

        
        
        // $abc = Users::find($inp_id)->first();
        $abc->name = $name;
        $abc->address = $address;
        $abc->email = $email;
        $abc->u_type = $u_type;
        $abc->phone_number = $phone_number;
        
        if ($request->hasfile('image')) {
                $imagePath = "storage/images/$abc->image";
                File::delete($imagePath);
                $images = $request->file('image');
                $filename = time() . '.' . $images->getClientOriginalExtension();
                $path = $request->image->storeAs('public/images', $filename);
                $abc->image = $filename;
         }
        $abc->save();


        \Session::flash('message', "your information updated");
        \Session::flash('alert-class', 'alert-info'); 
        return redirect('/userlist');
        
 }
 //delete user account controller
 public function delete_info($id){
    $user = Users::where('id', $id)->first();
    $user_login = Login::where('id', $id)->first(); 
    File::delete("storage/images/$user->image");
    $user->delete();
    $user_login->delete();
    \Session::flash('message', "Acount has deleted");
    \Session::flash('alert-class', 'alert-danger');
    return redirect('/userlist');
 }

 //pdf controller
 public function add_pdf(Request $request){
    $rules = [
        'name' => 'required|unique:docs|max:255',
        'pdf' => 'required|mimes:pdf',
    ];
    $error_massage = [
        'name.required' => 'Name Is Important, Enter It',
        'name.unique:docs' => 'Name Have To Be Unique',
        'pdf.required' => 'Where Your PDF???',
        'pdf.mimes:pdf' => 'You Should Be Choose A pdf File',

    ];
    $this->validate($request, $rules, $error_massage);
      
    
        if(empty($docs)) {  
            $docs_up = new Docs();
            if ($request->hasFile('pdf')) {
               $docs = $request->file('pdf');
               $filename = time() . '.' . $docs->getClientOriginalExtension();
               $path = $request->pdf->storeAs('public/pdf', $filename);
           }
           $docs_up->pdf = $filename;
           $docs_up->name= $request->input('name');
           $docs_up->save();

        //    echo "hi";die;

           \Session::flash('message', "Document has been uploaded"); 
           \Session::flash('alert-class', 'alert-success'); 

           return redirect('/docs');
        
        }
 }

 public function list_pdf(){
    $docs_show = Docs::all();
    return view('pages.docs', compact('docs_show'));
}
//edit
public function edit_infopdf($id) {

    $editPdf = Docs::where('id', $id)->first(); 
    // $users = Users::where('u_name', $u_name)->first();
    return view('pages.editPdf')->with('editPdf', $editPdf);
 }

public function edit_pdf(Request $request, $id){
    $rules = [
        'name' => 'required|unique:docs,id,.$id|max:255',
        'pdf' => 'nullable|mimes:pdf',
    ];
    $error_massage = [
        'name.required' => 'Name Is Important, Enter It',
        'name.unique:docs' => 'Name Have To Be Unique',
        'pdf.mimes:pdf' => 'You Should Be Choose A pdf File',

    ];
    $this->validate($request, $rules, $error_massage);

    $editPdf = Docs::where('id', $id)->first();
    
    if ($request->hasFile('pdf')) {
        File::delete("storage/pdf/$editPdf->pdf");
        $ePdf = $request->file('pdf');
        $filename = time() . '.' . $ePdf->getClientOriginalExtension();
        $path = $request->pdf->storeAs('public/pdf', $filename);
    }else{
        $filename = $editPdf->pdf;
    }
    $editPdf->pdf = $filename;
    $editPdf->name = $request->input('name');
    $editPdf->save();
    
    \Session::flash('message', "PDF has been updated");
    \Session::flash('alert-class', 'alert-info');
    return redirect('/docs');
}

// delete
public function delete_pdf($id){
    $docs = Docs::where('id', $id)->first();
    File::delete("storage/pdf/$docs->pdf");
    $docs->delete();
    \Session::flash('message', "PDF has been deleted");
    \Session::flash('alert-class', 'alert-danger');
    return redirect('/docs');
 }

 //print or download documents

 //in excel
 public function excel(){
    return Excel::download(new UsersExport, 'users.xlsx');
 }
 // in csv
 public function exportCSV()
 {
   return Excel::download(new UsersExport, 'users.csv');
 }
//in pdf
public function generatePDF(){
    $users=Users::all();
    $pdf = PDF::loadView('pages.usersl',['users'=>$users]);
    return $pdf->download('users.pdf');
}








    

   






























//         // register controllers start
//    public function RegisterView(){
//     return view('auth.register');
// }

// public function RegisterSubmit(Request $request){
 
//              // validation checker
//          $validatedData = $request->validate([
//              'email' => 'required|unique:users|max:255',
//              'password' => 'required|required_with:confirmed_password|same:confirmed_password|min:6',
//          ]);

//          // save in variable
//          $email = $request->input('email');
//          $password = $request->input('password');
//          $hashpassword = Hash::make($password);
     
//          //save in database of user
//          $users = Users::where("email",$email)->first();
         
//          if(empty($admin)) {  
//              $user = new Admin();
//              $user->email = $email;
//              $user->password = $hashpassword;
//              $user->save();

//              \Session::flash('message', "Account has been created"); 
//              \Session::flash('alert-class', 'alert-success'); 

//              return redirect('/alogin');
//          }
//          else{

//              \Session::flash('message', "Already You've Account. Try to Login"); 
//              \Session::flash('alert-class', 'alert-danger'); 
//              return redirect()->back();
//              }
             
     
     
//  }
//  // register controllers end

}