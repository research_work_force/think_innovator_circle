<?php

namespace App\Exports;

use App\Users;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
class UsersExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Users::select('name','phone_number','email','address','u_type')->get();
    }

    public function headings(): array
    {
        return [
            'Name',
            'Phone Number',
            'Email',
            'Address',
            'User Type'
        ];
    }

    public function map($user): array
    {
        return [
            $user->name,
            $user->phone_number,
            $user->email,
            $user->address,
            $user->u_type,
        ];
    }
}
